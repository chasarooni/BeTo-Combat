'use strict';
(factory => {
    Hooks.once('beto.library.loaded', beto => {
        Hooks.once('socketlib.ready', () => {
            beto.registerModule('combat.functions', factory(window.socketlib, beto));
        });
    });
})(beto => {
    const onBeforeSpellCast = [];
    const onSpellCast = [];
    const onConcentrationCheck = [];

    Hooks.once('setup', () => {
        combat.concentratorConditionName = game.cub?.active ? game.settings.get('combat-utility-belt', 'concentratorConditionName') : 'Concentrating';
    });

    libWrapper.ignore_conflicts('beto-combat', 'midi-qol', 'CONFIG.Item.documentClass.prototype.roll');
    libWrapper.register('beto-combat', 'CONFIG.Item.documentClass.prototype.roll', async function (wrapped, options) {
        const item = this;
        switch (item.data.data.target.units) {
            case 'touch':
                if (game.user.targets.size !== 1) {
                    ui.notifications.warn('You can only select 1 target');
                    return;
                }
                if (!beto.canInteract(beto.getTokenForActor(item.actor), beto.getOnlyInSet(game.user.targets))) {
                    ui.notifications.warn('The target is out of range');
                    return;
                }
                break;
        }
        switch (item.type) {
            case 'spell':
                if (!await beto.asyncEvery(onBeforeSpellCast, callback => callback(item, options))) {
                    return;
                }
                let result = await wrapped(options);
                for (const callback of onSpellCast) {
                    const response = await callback(item, result);
                    if (response !== undefined) {
                        result = response;
                    }
                }
                Hooks.callAll('beto.combat.onSpellCast', item, result, options);
                return result;
            case 'weapon':
                if (item.data.name === game.i18n.localize('midi-qol.concentrationCheckName')) {
                    await beto.asyncForEach(onConcentrationCheck, callback => callback(item, options));
                }
                return wrapped(options);
            default:
                return wrapped(options);
        }
    }, 'MIXED');

    const combat = {
        concentratorConditionName: 'Concentrating',
        socket: socketlib.registerModule('beto-combat'),
        canBeThrown: item => item.data.data.properties.thr,
        usesAmmunition: item => item.data.data.properties.amm,
        getWeaponType: item => combat.usesAmmunition(item) ? 'ranged' : combat.canBeThrown(item) ? 'throwable' : 'melee',
        inCloseCombat: (actor, distanceToTarget) => distanceToTarget <= beto.getReach(actor),
        isThrown: (item, distanceToTarget) => combat.canBeThrown(item) && !combat.inCloseCombat(item.actor, distanceToTarget),
        onBeforeSpellCast: callback => onBeforeSpellCast.push(callback),
        onSpellCast: callback => onSpellCast.push(callback),
        onConcentrationCheck: callback => onConcentrationCheck.push(callback),
    };
    return combat;
});
