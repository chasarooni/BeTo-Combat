'use strict';
(factory => {
    window.Hooks.once('beto.library.loaded', beto => {
        beto.registerModule('combat.spells', factory(beto));
    });
})(beto => {

    Hooks.on('deleteActiveEffect', activeEffect => {
        if (activeEffect.data.label !== beto.concentratorConditionName) {
            return;
        }
        Hooks.callAll('beto.combat.spells.onConcentrationLost', activeEffect.parent, activeEffect);
    });

    return {};
});
