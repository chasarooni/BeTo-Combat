'use strict';
(factory => {
    window.Hooks.once('beto.library.loaded', async beto => {
        const combat = await beto.getModule('combat.functions');
        beto.registerModule('combat.spells', factory(combat));
    });
})(combat => {
    combat.socket.register('notifyReactionModal', (senderName) => {
        ui.notifications.info(senderName + ' is getting a reaction');
    });

    Hooks.on('getReactionDialogHeaderButtons', () => {
        combat.socket.executeAsGM('notifyReactionModal', game.user.character.name);
    });

    return {};
});
