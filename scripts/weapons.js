'use strict';
(factory => {
    window.Hooks.once('beto.library.loaded', async beto => {
        const combat = await beto.getModule('combat.functions');
        beto.registerModule('combat.weapons', factory(beto, combat));
    });
})((beto, combat) => {
    const getMinRange = item => item.data.data.range.long ? item.data.data.range.value : 0;
    const getMaxRange = item => item.data.data.range.long ? item.data.data.range.long : item.data.data.range.value;
    const canHitTarget = (actor, weaponType, distanceToTarget, maxRange) => {
        if (weaponType === 'ranged' && combat.inCloseCombat(actor, distanceToTarget)) {
            ui.notifications.warn('The target is too close');
            return false;
        }
        if (distanceToTarget > maxRange) {
            ui.notifications.warn('The target is out of range');
            return false;
        }
        return true;
    };
    const removeAmmunition = item => {
        const ammunition = item.actor.data.items.filter(inventoryItem => inventoryItem.type === 'consumable' && inventoryItem.data.data.consumableType === 'ammo');
        switch (ammunition.length) {
            case 0:
                ui.notifications.warn('You have no ammunition');
                return false;
            case 1:
                ammunition[0].data.data.quantity--;
                ammunition[0].update({ data: { quantity: ammunition[0].data.data.quantity } });
                return true;
            default:
                ui.notifications.error('Multiple ammunition is not yet implemented.');
                // TODO @JB ask for ammunition and restart the roll workflow (this might require a wrapper)
                return false;
        }
    };

    Hooks.on('midi-qol.preAttackRoll', (item, workflow) => {
        const minRange = getMinRange(item);
        const maxRange = getMaxRange(item);
        const usesAmmunition = combat.usesAmmunition(item);
        const weaponType = combat.getWeaponType(item);

        let failure = false;
        workflow.targets.forEach(target => {
            if (failure) {
                return; // every() is not a function on the Set so I can't break half way through.
            }
            const distanceToTarget = beto.calculateDistance(beto.getTokenForActor(item.actor), target);

            if (!canHitTarget(item.actor, weaponType, distanceToTarget, maxRange)) {
                failure = true;
                return;
            }

            if (usesAmmunition && !removeAmmunition(item)) {
                failure = true;
                return;
            }

            if (combat.isThrown(item, distanceToTarget) && !Hooks.call('beto.combat.removeThrownWeapon', item)) {
                item.data.data.quantity--;
                item.update({ data: { quantity: item.data.data.quantity } });
            }

            if (weaponType !== 'melee' && distanceToTarget > minRange) {
                workflow.rollOptions.disadvantage = true;
            }
        });
        return !failure;
    });

    return {};
});
